<div align="center">
<h1>tone command line</h1>
</div>

English | [简体中文](README-zh_CN.md)

## Install

```bash
make install
```

## Usage

### Usinghelp

    tone --help/-h

### Command description

    tone [-h] [-v] {list,run,fetch,install,uninstall,archive}{testsuite_name}

- tone list：Displays a list of supported TestSuites
- tone list testsuite_name：Displays all testconf under the specified testsuite
- tone run testsuite_name：Run all testconf under the specified testsuite
- tone run testconf_name：Run the specified testconf
- tone fetch testsuite_name：Pull the specified testsuite,according to GIT_URL/WEB_URL in the tests/testsuite/install.sh
- tone install testsuite_name：Installs the specified testsuite
- tone uninstall testsuite_name：Uninstalls the specified testsuite
- tone archive：In an offline test, you can run this command to save the test result as a tar package and upload the test result to the T-One platform for analysis

## 更多文档

[The complete process of running the test tool](docs/manual-zh_CN.md)

[How to integrate new testing tools for tone-cli](docs/add-testsuite-zh_CN.md)