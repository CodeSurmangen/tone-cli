#!/bin/env python3
# coding=utf-8

import os
import sys

common_head = """#!/bin/bash
# Avaliable environment:
#
# Download variable:
# WEB_URL=
# GIT_URL=

"""


def write2file(fn, string):
    with open(fn, "w+") as fh:
        fh.write(string)


def init_install(fn):
    install = common_head + """
# fetch()

build()
{
    :
}

install()
{
    :
}
    """
    write2file(fn, install)


def init_run(fn):
    run = common_head + """
run()
{
    echo "Please implement your test run step at here"
}

parse()
{
    $TONE_BM_SUITE_DIR/parse.py
}
    """
    write2file(fn, run)


def init_parse(fn):
    parse = """#!/bin/env python3
import sys
import re

with sys.stdin as file:
    # '''If you don't need to use stdin just pass it'''
    pass
    # '''If you want to proces stdin can use below codes'''
    # for line in file:
    #     pass
# functional output should be:
#   Tone_default_test: Pass/Fail/Skip
# benchmark test output should be:
# performance_indext: value unit
# Eg.
#   bond_width: 10.0 MBps
# unit is optional
# Eg.
#   score: 100.0
print("Tone_default_test: Pass")

"""
    write2file(fn, parse)
    os.chmod(fn, 0o755)


def init_readme(fn, test):
    readme = "# " + test + """
## Description

About the test suite

## Homepage
[http://](http://)

## Version

## Category

functional/benchmarck/stress

## Parameters

- __value__ : about the value

## Results

```
result: value
```

## Manual Run
```
step n: xxxx

```

"""
    write2file(fn, readme)


def init_test_directory(path):
    if not os.path.exists(path):
        os.makedirs(path)


if __name__ == '__main__':
    test = sys.argv[1]
    test_path = os.path.join(".", "tests", test)
    init_test_directory(test_path)
    init_install(os.path.join(test_path, 'install.sh'))
    init_run(os.path.join(test_path, 'run.sh'))
    init_parse(os.path.join(test_path, 'parse.py'))
    init_readme(os.path.join(test_path, 'Readme.md'), test)
    print("Done. init test: {}".format(test_path))
