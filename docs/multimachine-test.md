## 多机测试使用手册

### 准备工作

```
1. 准备2台环境，作为local端和server端;
2. local上增加免密操作，使local端环境能直联server端；
3. 2台环境上的工作目录${WorkSpace}保持一致； 
4. 服务运行前，查看防火墙是否关闭，需要关闭防火墙：
    systemctl status firewalld
    systemctl stop firewalld
5. 运行以下步骤，仅需要在 local 端上执行即可；

```


### 安装

```
local端安装：
cd ${WorkSpace}
git clone https://gitee.com/anolis/tone-cli.git
cd tone-cli
make install

在local端上进行server端工具安装(后续步骤类似，不做赘述)：
ssh -o StrictHostKeyChecking=no root@${server_ip} "export SERVER=${server_ip}  && cd ${WorkSpace} && git clone https://gitee.com/anolis/tone-cli.git && cd ./tone-cli && make install"

```


### 下载测试套件(以下以 iperf 为例)

```
tone fetch iperf

ssh -o StrictHostKeyChecking=no root@${server_ip} "export SERVER=${server_ip}  && cd cd ${WorkSpace}/tone-cli && tone fetch iperf"

```


### 安装测试套件

```
tone install iperf

ssh -o StrictHostKeyChecking=no root@${server_ip} "export SERVER=${server_ip}  && cd cd ${WorkSpace}/tone-cli && tone install iperf"

```


### 查看测试套件

```
tone list iperf

ssh -o StrictHostKeyChecking=no root@${server_ip} "export SERVER=${server_ip}  && cd cd ${WorkSpace}/tone-cli && tone list iperf"

```

##### 例如：

```
tone list iperf

runtime protocol testconf
300     udp      iperf:runtime=300,protocol=udp
300     tcp      iperf:runtime=300,protocol=tcp

```


### 运行测试套件

```
1. 300-udp
tone run iperf:runtime=300,protocol=udp
2. 300-tcp
tone run iperf:runtime=300,protocol=tcp

server端可以查看服务进程:

ps -aux | grep iperf

```

### 查看结果

```
位置： ${WorkSpace}/tone-cli/result/测试套件/测试集/运行次数/

以测试套件 iperf的 300-udp 测试集为例：
cd ${WorkSpace}/tone-cli/result/iperf/300-udp/3

[root@e18g09524 3]# tree -L 2
.
├── env.sh  # 运行后的环境变量
├── iperf.json  # 
├── result.json  # 结果文件
├── runstatus.json  # 运行结果状态值文件
├── stderr.log  # 标准错误输出日志
├── stdout.log  # 标准输出日志
├── sysinfo
│   ├── post  # 运行后系统信息
│   └── pre  # 运行前系统信息
├── testinfo
│   └── cpu_model  # 运行环境cpu型号
├── testinfo.sh # 收集环境信息
└── var.sh  # 运行后的环境变量

```


### 问题及解决方法

1. 报错:关键字串"Operation not permitted"

```text
ssh -o StrictHostKeyChecking=no root@$server_ip "export SERVER=$server_ip && cd /date-test/tone-cli && tone install iperf"
Mar 07 17:48:03 <tone> [ERROR]: [Errno 1] Operation not permitted
Mar 07 17:48:03 <tone> [ERROR]: [Errno 1] Operation not permitted

Test Suite   : iperf
Test Conf    : Not needed at current stage
Test Setting :
Test Run Path: /date-test/tone-cli/run/iperf
Test Result  : /date-test/tone-cli/result/iperf/stdout.log
Test Cache   : /date-test/tone-cli/cache/iperf

Traceback (most recent call last):
  File "/date-test/tone-cli/core/modulemanager.py", line 77, in map_method
    method(*argv)
  File "/date-test/tone-cli/core/modules/subcommand/run/Worker.py", line 19, in run
    testcmd.install()
  File "/date-test/tone-cli/core/utils/tests.py", line 356, in install
    self._run(command, halted_on_fail=True)
  File "/date-test/tone-cli/core/utils/tests.py", line 256, in _run
    is_quiet=self.config.is_quiet
  File "/date-test/tone-cli/core/utils/tests.py", line 127, in __init__
    is_quiet=is_quiet
  File "/date-test/tone-cli/core/utils/exec_cmd.py", line 76, in __init__
    preexec_fn=os.setpgid(0, 0),
PermissionError: [Errno 1] Operation not permitted

```

解决：
    需要2台环境之间添加免密

2. 报错: 关键字串"unable to connect to server: Connection refused" 或者 ""/disk1/test/tone-cli/run/iperf/bin/iperf3" No such file or directory"


```

numactl: execution of "/disk1/test/tone-cli/run/iperf/bin/iperf3" No such file or directory
Error: The return code of setup() in run.sh is not 0
numactl -C 0 -m 0 iperf3 -t 300 -f M -J -c $server_ip -u
warning: Report format (-f) flag ignored with JSON output (-J)
{
        "start":        {
                "connected":    [],
                "version":      "iperf 3.12",
                "system_info":  "Linux xxxxx 4.19.91-26.3.an8.x86_64 #1 SMP Mon Aug 29 17:36:17 CST 2022 x86_64"
        },
        "intervals":    [],
        "end":  {
        },
        "error":        "unable to connect to server: Connection refused"
}
client_cmd has finished
Traceback (most recent call last):
  File "/disk1/test3.7/tone-cli/tests/iperf/parse.py", line 11, in <module>
    targetJson=results.get("end").get("streams")[0]
TypeError: 'NoneType' object is not subscriptable
Skip line: Traceback (most recent call last):
Skip line: TypeError: 'NoneType' object is not subscriptable
Mar 07 17:56:27 <tone> [WARNING]: /disk1/test3.7/tone-cli/result/iperf/300-udp/1/sysinfo/post already exists
Mar 07 17:56:27 <tone> [WARNING]: /disk1/test3.7/tone-cli/result/iperf/300-udp/1/sysinfo/post already exists
Test running: Done

```

解决：
    ①需要排查 local 端和 server 端 $WorkSpace目录及iperf工具安装是否保持一致；
    ②需要排查 local 端和 server 端 防火墙是否未关闭；
