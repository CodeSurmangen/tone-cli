# intel-lkvs
## Description
The Linux Kernel Validation Suite (LKVS) is a Linux Kernel test suite. It is a working project created by the Intel Core Linux Kernel Val Team. The purpose is to improve the quality of Intel-contributed Linux Kernel code. Furthermore, it shows the customer how to use or validate the features.

## Homepage
https://gitee.com/anolis/intel-lkvs

## Category
Functional

## Results
```
... ...
MSR_ARCH_LBR_INFO_0_write_1.0:PASS
MSR_ARCH_LBR_CTL_write_1.0:PASS
MSR_ARCH_LBR_DEPTH_read_1.0:PASS
MSR_ARCH_LBR_FROM_0_read_1.0:PASS
MSR_ARCH_LBR_TO_0_read_1.0:PASS
MSR_ARCH_LBR_TO_0_write_1.0:PASS
MSR_STAR_read_1.0:PASS
MSR_LSTAR_read_1.0:PASS
MSR_SYSCALL_MASK_read_1.0:PASS
MSR_GS_BASE_read_1.0:PASS
MSR_KERNEL_GS_BASE_read_1.0:PASS
MSR_TSC_AUX_read_1.0:PASS
xstate around process signal handling:PASS
xstate of child process should be same as xstate of parent:PASS
parent xstate should be same after context switch:PASS
```
## Manual Run
* TDX-Compliance
```
echo all 1.0 > /sys/kernel/debug/tdx-tests
cat /sys/kernel/debug/tdx-tests
```

* XSave
./xstate_64
