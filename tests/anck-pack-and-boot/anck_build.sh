#!/bin/bash

KERNEL_CI_REPO_URL=$2
KERNEL_CI_REPO_BRANCH=$3
KERNEL_CI_PR_ID=$4
CK_BUILDER_REPO="https://gitee.com/src-anolis-sig/ck-build.git"

show_result()
{
    local case_name=$1
    local ret=$2
    local show_str=$3

    [ -n "$show_str" ] && echo "$show_str"
    if [ $ret -ne 0 ]; then
        echo "$case_name: fail"
        exit 1
    else
        echo "$case_name: pass"
        exit 0
    fi
}


if [ $(arch) == "x86_64" ]; then
    kernel_arch="x86"
elif [ $(arch) == "aarch64" ]; then
    kernel_arch="arm64"
else
    show_result $1 1 "Error: Not supported arch"
fi

[ -n "$KERNEL_CI_REPO_URL" ] || {
    show_result $1 1 "Error: KERNEL_CI_REPO_URL is not set"
}
[ -n "$KERNEL_CI_REPO_BRANCH" ] || {
    show_result $1 1 "Error: KERNEL_CI_REPO_BRANCH is not set"
}
[ -n "$KERNEL_CI_PR_ID" ] || {
    echo "Warning: KERNEL_CI_PR_ID is not set"
}

builder_dep_pkg="glibc-static flex bison elfutils-libelf-devel openssl-devel dwarves"
[ -n "$CK_BUILDER_BRANCH" ] || {
    if echo "$KERNEL_CI_REPO_BRANCH" | grep -q "4.19"; then
        CK_BUILDER_BRANCH="an8-4.19"
    elif echo "$KERNEL_CI_REPO_BRANCH" | grep -q "5.10"; then
        CK_BUILDER_BRANCH="an8-5.10"
    elif echo "$KERNEL_CI_REPO_BRANCH" | grep -q "6.1"; then
        CK_BUILDER_BRANCH="an23-6.1"
        builder_dep_pkg="$builder_dep_pkg bc elfutils-devel libnl3-devel perl-devel rsync"
    elif echo "$KERNEL_CI_REPO_BRANCH" | grep -q "6.6"; then
        CK_BUILDER_BRANCH="an23-6.6"
#         cat > /etc/yum.repos.d/build.repo <<EOF
# [build]
# name=build
# baseurl=http://8.131.87.1/kojifiles/repos/dist-an23-build/latest/$(uname -m)/
# enabled=1
# gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ANOLIS
# gpgcheck=0
# EOF
        builder_dep_pkg="$builder_dep_pkg bc elfutils-devel libnl3-devel perl-devel rsync perl-generators"
    else
        show_result $1 1 "Error: KERNEL_CI_REPO_BRANCH:$KERNEL_CI_REPO_BRANCH is not support"
    fi
}


job_num=$(cat /proc/cpuinfo | grep processor | wc | awk {'print $1-1'})

mkdir -p /anck_build
cd /anck_build
anck_repo=$(basename $KERNEL_CI_REPO_URL .git)
[ -d "$anck_repo" ] && {
    echo "$anck_repo exist, try to remove it..."
    rm -rf $anck_repo
}

echo "===> Clone kernel repository..."

# 设置重试次数和延迟时间
retry_count=3
retry_delay=3

# 通用重试函数
retry() {
    local n=1
    local max=$1
    local delay=$2
    shift 2
    local cmd="$@"
    echo "===> Executing command: $cmd"
    while true; do
        if "$@"; then
            echo "Command succeeded: $cmd"
            break
        else
            if [[ $n -lt $max ]]; then
                ((n++))
                echo "Command failed: $cmd. Attempt $n/$max:"
                sleep $delay
            else
                echo "The command has failed after $n attempts: $cmd"
                return 1
            fi
        fi
    done
}


# 克隆仓库的重试逻辑
if ! retry $retry_count $retry_delay git clone --depth 1 -b $KERNEL_CI_REPO_BRANCH $KERNEL_CI_REPO_URL; then
    echo "Failed to clone the repository after $retry_count attempts."
    exit 1
fi

# 检查是否设置KERNEL_CI_PR_ID
if [ -z "$KERNEL_CI_PR_ID" ]; then
    echo "===> KERNEL_CI_PR_ID not set, Skip apply patch"
    cd $anck_repo
else
    # 生成补丁URL
    patch_url="${KERNEL_CI_REPO_URL/\.git/}/pulls/${KERNEL_CI_PR_ID}.patch"
    echo "===> Get the patch from: $patch_url"
    patch_file=$(basename $patch_url)

    [ -f "$patch_file" ] && rm -f "$patch_file"

    # 下载补丁的重试逻辑
    if ! retry $retry_count $retry_delay wget $patch_url; then
        echo "Failed to download patch after $retry_count attempts."
        exit 1
    else
        echo "===> Downloaded patch successfully"
    fi

    echo "===> Patch content:"
    cat "$patch_file"
    cd $anck_repo || { echo "===> Could not change to directory: $anck_repo" ; exit 1; }
    echo "===> Apply patch: $patch_url"
    echo "CMD: git config user.email \"test@test.com\""
    echo "CMD: git config user.name \"test\""
    git config user.email "test@test.com"
    git config user.name "test"
    echo "CMD: git am ../$(basename $patch_url)"
    if git am "../$patch_file"; then
        echo "Apply patch pass: $patch_url"
    else
        echo "Apply patch fail: $patch_url"
        echo "===> git am failure log:"
        git am --abort
        git apply --check "../$patch_file" 2>&1 | tee ../apply_patch_error.log
        echo "===> git apply check log:"
        cat ../apply_patch_error.log
        show_result $1 1 "Apply patch fail: $patch_url"
    fi
    echo "===> Apply patch done"
fi


echo "===> Install related packages..."
if [[ "$KERNEL_CI_REPO_BRANCH" == *"4.19"* || "$KERNEL_CI_REPO_BRANCH" == *"6.1"* ]]; then
    echo "CMD: yum install -y $builder_dep_pkg"
    yum install -y $builder_dep_pkg
else
    if [[ "$KERNEL_CI_REPO_BRANCH" == *"5.10"* || "$KERNEL_CI_REPO_BRANCH" == *"6.6"* ]]; then
        echo "Changing directory and building kernel spec..."
        pushd anolis/ 
        make dist-genspec && yum builddep -y output/kernel.spec
        popd
    else
        echo "KERNEL_CI_REPO_BRANCH does not match any known versions."
    fi
fi
echo "===> Install packages done"

if [ $1 == "build_allyes_config" ]; then
    echo "== Build Kernel with allyesconfig =="
    echo "make clean && make allyesconfig && make -j $job_num -s && make modules -j $job_num -s"
    make clean && make allyesconfig && make -j $job_num -s && make modules -j $job_num -s
    show_result $1 $?
elif [ $1 == "build_anolis_defconfig" ]; then
    echo "== Build Kernel with anolis_defconfig =="
    echo "CMD: make clean && make anolis_defconfig && make olddefconfig && make -j $job_num -s &&  make modules -j $job_num -s"
    make clean && make anolis_defconfig && make olddefconfig && make -j $job_num -s &&  make modules -j $job_num -s
    show_result $1 $?
elif [ $1 == "build_allno_config" ]; then
    echo "== Build Kernel with allnoconfig =="
    echo "CMD: make clean && make allnoconfig && make -j $job_num -s"
    make clean && make allnoconfig && make -j $job_num -s
    show_result $1 $?
elif [ $1 == "build_anolis_debug_defconfig" ]; then
    echo "== Build Kernel with anolis-debug_defconfig =="
    echo "CMD: make clean && make anolis-debug_defconfig && make olddefconfig && make -j $job_num -s &&  make modules -j $job_num -s"
    make clean && make anolis-debug_defconfig && make olddefconfig && make -j $job_num -s &&  make modules -j $job_num -s
    show_result $1 $?
elif [ $1 == "check_Kconfig" ]; then
    echo "== Check kconfig =="
    check_status=0
    if echo "$KERNEL_CI_REPO_BRANCH" | grep -qE "6\.6|5\.10"; then
        cd /anck_build/${anck_repo}/anolis
        ARCH=${kernel_arch} make dist-configs-check
        check_status=$?
    else
        for cfg in $(ls arch/${kernel_arch}/configs/anolis_*);
        do
            echo "Check $cfg"
            echo "CMD: cp -f $cfg .config && make ARCH=${kernel_arch} listnewconfig | grep -E '^CONFIG_' > .newoptions"
            cp -f $cfg .config && make ARCH=${kernel_arch} listnewconfig | grep -E '^CONFIG_' > .newoptions
            [ -s .newoptions ] && cat .newoptions && check_status=1;
        done
    fi
    show_result $1 $check_status
elif [ $1 == "anck_rpm_build" ]; then
    echo "== Build Kernel RPM =="
    echo "Get ck builder..."
    cd /anck_build
    ck_builder=$(basename $CK_BUILDER_REPO .git)
    [ -d "$ck_builder" ] && rm -rf $ck_builder
    git clone -b $CK_BUILDER_BRANCH $CK_BUILDER_REPO || return 1
    [ _"$ck_builder" != "_ck-build" ] && ln -s $ck_builder ck-build
    cd $ck_builder
    ln -sf ../${anck_repo} cloud-kernel
    if [ "$CK_BUILDER_BRANCH" == "an23-6.6" ]; then
        pushd cloud-kernel/anolis || {
            echo "Failed to enter directory: cloud-kernel/anolis"
            exit 1
        }

        make dist-genspec || {
            echo "make dist-genspec failed"
            exit 1
        }
        popd

        if [ -f cloud-kernel/anolis/output/kernel.spec ]; then
            cp cloud-kernel/anolis/output/kernel.spec "$(pwd)"
            echo "kernel.spec has been copied to the current directory."
        else
            echo "kernel.spec file not found in output directory."
            exit 1
        fi
        echo $(pwd)
    fi

    [ -d "outputs" ] && rm -rf outputs
    yum install -y yum-utils

    yum-builddep -y kernel.spec
    if [ "$CK_BUILDER_BRANCH" == "an23-6.6" ]; then
        echo "CMD:  yum downgrade  -y binutils-2.39*"
        yum downgrade -y binutils-2.39*
    fi
    echo "Build ANCK..."
    [ -n "$BUILD_EXTRA" ] || export BUILD_EXTRA="debuginfo"
    sh build.sh
    show_result $1 $?
fi
