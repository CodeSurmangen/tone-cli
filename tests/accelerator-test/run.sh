#!/bin/bash

export log_dir="$TONE_BM_RUN_DIR/tests/accelerator/logs"
export work_dir="$TONE_BM_RUN_DIR/tests/accelerator"

setup(){
	[ -n "$PKG_CI_ABS_RPM_URL" ] || {
		echo "PKG_CI_ABS_RPM_URL should not be empty!"
		exit
	}
}

deal_log(){
	pushd $log_dir
		for log_deal in $(ls ./ | grep "_test.log")
		do
			local testcase=$(echo "$log_deal" | sed 's#.log##')
			sed -i "s/^/${testcase}:/" $log_deal
		done
	popd
}

run()
{
	pushd $work_dir
		if [ "$case" != "all" ];then
			local testcase="${case}.sh"
			bash ./runtest.sh -d ./tests -t $testcase
		elif [ "$case" == "all" ];then
			bash ./runtest.sh -d ./tests
		fi
	popd
	deal_log
}

parse()
{
	cp -arf $log_dir/* ${TONE_CURRENT_RESULT_DIR}/
	$TONE_BM_SUITE_DIR/parse.awk $log_dir/*_test.log
}
