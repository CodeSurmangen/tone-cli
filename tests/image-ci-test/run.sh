#!/bin/bash
# Avaliable environment variables:
# $USERNAME: host username, only one or username list seperated by ','
# $PASSWROD: host password, only one or password list seperated by ','
# $HOST_IP: host ip list seperated by ','
# $HOST_NAME: host name list seperated by ','
group_array=$(cat $TONE_CONF_DIR/functional/image-ci-test.conf | grep -v ^group$)

setup()
{
    if python3 -m pip list | grep avocado-framework; then
        python3 -m pip uninstall avocado-framework avocado-framework-plugin-varianter-yaml-to-mux -y
    fi
    python3 -m pip install --upgrade pip
    pip3 install --ignore-installed PyYAML
	python3 -m pip install -r $TONE_BM_RUN_DIR/requirements.txt || {
        pip3 config set global.index-url http://mirrors.aliyun.com/pypi/simple/
        python3 -m pip install -r $TONE_BM_RUN_DIR/requirements.txt --ignore-installed --trusted-host mirrors.aliyun.com
    }
}

run()
{
    [ -n "$group" ] || {
        echo "No group in specfile"
        return 1
    }
    [[ "$group_array" =~  "$group" ]] || {
        echo "$group is not supported"
        return 1
    }

    export PYTHONPATH=$TONE_BM_RUN_DIR/tests
    IFS=', ' read -ra run_tags <<< "$TONE_TESTCASES"
    for tag in "${run_tags[@]}"; do
        if [ -n "$tag" ] && [ "$tag" != "null" ] && [ "$tag" != "container_default" ]; then
            option+="-t $tag "
        fi
    done
    generate_yaml
    if [ "$group" == "vhd_smoke_test" ]; then
        for vhd_case in $(find $TONE_BM_RUN_DIR/tests -type f -name "*.py" -exec grep -l "tags=.*vhd_img" {} +); do
            avocado -V run --nrunner-max-parallel-tasks 1 $vhd_case -m $TONE_BM_RUN_DIR/hosts.yaml -t vhd_img
            casename=$(echo $vhd_case | xargs basename | cut -d. -f1)
            cp -rH $HOME/avocado/job-results/latest/test-results/ $TONE_CURRENT_RESULT_DIR/$casename
        done
    elif [ "$group" == "keentune_application_container_func_test" ]; then
        if echo "$CONTAINER_SERVICE" |grep -q "optimized"; then
            service_name=${CONTAINER_SERVICE/optimized/keentune}
        else
            service_name=$CONTAINER_SERVICE
        fi
        avocado -V run --nrunner-max-parallel-tasks 1 $TONE_BM_RUN_DIR/tests/keentune/tc_container_${service_name}_001.py --mux-yaml $TONE_BM_RUN_DIR/hosts.yaml
        cp -rH $HOME/avocado/job-results/latest/test-results/ $TONE_CURRENT_RESULT_DIR/$casename
    else
        yaml_add_registry
        # yaml_add_engine
        c_engine=(`echo $CONTAINER_ENGINE | tr '/' ' '`)
cat >> hosts.yaml << EOF
engines: !mux
EOF

        #container_default  默认
        for vhd_case in $(find $TONE_BM_RUN_DIR/tests -type f -name "*.py" -exec grep -l "tags=.*container_default" {} +| xargs grep -LE "tags=.*_container_default" ); do
            echo $vhd_case >> /tmp/case_file.txt
        done
        if [ "$group" != "container_default_group" ]; then
            run_tags+=("${group}_default")
        fi
        for element in "${run_tags[@]}"; do
            for vhd_case in $(find $TONE_BM_RUN_DIR/tests -type f -name "*.py" -exec grep -l "tags=.*$element" {} +); do
                echo $vhd_case >> /tmp/case_file.txt
            done
        done

        test_cases=""
        for vhd_case in $(sort /tmp/case_file.txt | uniq); do
            test_cases="$test_cases $vhd_case"
        done

        for ((i = 0; i < ${#c_engine[@]}; i++)); do
            if grep -E "engine[0-9]+:" hosts.yaml > /dev/null; then
                sed -i "s/engine[0-9]:/engine$i:/g" hosts.yaml
                sed -i "s/engine: .*/engine: ${c_engine[$i]}/g" hosts.yaml
            else
cat >> hosts.yaml << EOF
    engine$i:
        engine: ${c_engine[$i]}
EOF
            fi
            # 传当前的测试engine给avocado JobPre plugin
            export TEST_ENGINE=${c_engine[$i]}
            avocado -V run --nrunner-max-parallel-tasks 1 $test_cases -m $TONE_BM_RUN_DIR/hosts.yaml 
            cp -rH $HOME/avocado/job-results/latest/test-results/ $TONE_CURRENT_RESULT_DIR/
        done
        
        rm -rf /tmp/case_file.txt
    fi

}

parse()
{
    $TONE_BM_SUITE_DIR/parse.py
}

generate_yaml()
{
    username=(`echo $USERNAME | tr '/' ' '`)
    password=(`echo $PASSWORD | tr '/' ' '`)
    host_ip=(`echo $HOST_IP | tr '/' ' '`)
    host_name=(`echo $HOST_NAME | tr '/' ' '`)
cat > hosts.yaml << EOF
hosts: !mux
EOF
    for ((i = 0; i < ${#host_ip[@]}; i++)); do
cat >> hosts.yaml << EOF
    ${host_name[$i]}$i:
        remote: ${host_ip[$i]}
EOF
        if [[ ${#username[@]} -eq 1 ]]; then
cat >> hosts.yaml << EOF
        username: $username
        password: $password
EOF
        else
cat >> hosts.yaml << EOF
        username: ${username[$i]}
        password: ${password[$i]}
EOF
        fi
    done
}

yaml_add_registry()
{
    registry_addr=(`echo $REGISTRY_ADDR | tr '#' ' '`)
cat >> hosts.yaml << EOF
registries: !mux
EOF
    for ((i = 0; i < ${#registry_addr[@]}; i++)); do
cat >> hosts.yaml << EOF
    registry$i:
        registry: ${registry_addr[$i]}
EOF
    done
}

yaml_add_engine()
{
    c_engine=(`echo $CONTAINER_ENGINE | tr '/' ' '`)
cat >> hosts.yaml << EOF
engines: !mux
EOF
    for ((i = 0; i < ${#c_engine[@]}; i++)); do
cat >> hosts.yaml << EOF
    engine$i:
        engine: ${c_engine[$i]}
EOF
    done
}
