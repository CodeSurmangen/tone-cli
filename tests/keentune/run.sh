#!/bin/bash
# Avaliable environment:
#
# Download variable:
# WEB_URL=
# GIT_URL=

. $TONE_ROOT/lib/testinfo.sh
. $TONE_ROOT/lib/common.sh
nginx_conf=/etc/nginx/nginx.conf
nr_open_default=$(cat /proc/sys/fs/nr_open)


get_keentune_code()
{
    cd $TONE_BM_RUN_DIR/
    rm -rf acops-new

    if [ "$code_source" == "gitee" ];then
        mkdir acops-new
        cd acops-new

        keentune_addr="https://gitee.com/anolis/keentuned.git"
        keentune_brain_addr="https://gitee.com/anolis/keentune_brain.git"
        keentune_target_addr="https://gitee.com/anolis/keentune_target.git"
        keentune_bench_addr="https://gitee.com/anolis/keentune_bench.git"

        git clone -b $keentune_branch $keentune_addr
        git clone -b $brain_branch $keentune_brain_addr
        git clone -b $target_branch $keentune_target_addr
        git clone -b $bench_branch $keentune_bench_addr

        cd keentuned
        cp test ../ -r
        cd ..

        mv keentune_brain keentune-brain
        mv keentune_target keentune-target
        mv keentune_bench keentune-bench
    else
        return 1
    fi
}

build_keentune()
{
    cd $TONE_BM_RUN_DIR/acops-new

    cd keentuned
    go mod vendor
    sh misc/install.sh

    cd ../keentune-brain
    python3 setup.py install

    cd ../keentune-target
    python3 setup.py install

    cd ../keentune-bench
    python3 setup.py install

    cd $TONE_BM_RUN_DIR/
}

clear_keentune_env()
{
    rm -rf /usr/local/lib/python3.6/site-packages/keentune*
    rm -rf /usr/local/lib/python3.6/site-packages/brain
    rm -rf /usr/local/lib/python3.6/site-packages/target
    rm -rf /usr/local/lib/python3.6/site-packages/bench
    rm -f /usr/local/bin/keentune*
    ps -ef|grep -E 'keentuned|keentune-brain|keentune-target|keentune-bench'|grep -v grep|awk '{print $2}'| xargs -I {} kill -9 {}
}

clear_log_files()
{
    rm -rf /var/log/bench*
    rm -rf /var/log/brain*
    rm -rf /var/log/target*
    rm -rf /var/log/keentune*
    rm -rf /var/keentune/backup/param_set*
}

# this function can be used when keentune is running
clear_keentune_job()
{
    echo y | keentune param delete --job param1
    echo y | keentune sensitize delete --job param1
    echo y | keentune param delete --job sensitize1
    echo y | keentune sensitize delete --job sensitize1
}

setup()
{
    logger clear_keentune_env
    logger clear_log_files
    logger "get_keentune_code || exit 1"
    logger "build_keentune || exit 1"
}

run_fail()
{
    cp $TONE_BM_RUN_DIR/keentuned-$log_suffix $TONE_CURRENT_RESULT_DIR/
    cp $TONE_BM_RUN_DIR/keentune-brain-$log_suffix $TONE_CURRENT_RESULT_DIR/
    cp $TONE_BM_RUN_DIR/keentune-bench-$log_suffix $TONE_CURRENT_RESULT_DIR/
    cp $TONE_BM_RUN_DIR/keentune-target-$log_suffix $TONE_CURRENT_RESULT_DIR/
    exit 1
}

run()
{
    cd $TONE_BM_RUN_DIR/
    log_suffix="$code_source-$test_branch-log-$(date +"%Y-%m-%d-%H-%M-%S")"
    keentune-brain > keentune-brain-$log_suffix 2>&1 &
    keentune-bench > keentune-bench-$log_suffix 2>&1 &
    keentune-target > keentune-target-$log_suffix 2>&1 &
    keentuned > keentuned-$log_suffix 2>&1 &

    sleep 5
    ps -ef|grep -w keentuned | grep -v grep || run_fail
    ps -ef|grep -w keentune-brain | grep -v grep || run_fail
    ps -ef|grep -w keentune-target | grep -v grep || run_fail
    ps -ef|grep -w keentune-bench | grep -v grep || run_fail

    clear_keentune_job

    logger cd $TONE_BM_RUN_DIR/acops-new/test

    echo -e "Start test:python3 main.py\nTests will take several minutes, pls wait"
    python3 main.py
    if [ $? -ne 0 ]; then
        cd $TONE_BM_RUN_DIR/
        echo "ERROR:python3 main.py failed"
        run_fail
    fi

    cd $TONE_BM_RUN_DIR/
}

parse()
{
    awk -f $TONE_BM_SUITE_DIR/parse.awk
}

teardown()
{
    [ -s "${nginx_conf}_bak" ] && \cp ${nginx_conf}_bak $nginx_conf
    systemctl stop nginx
    clear_keentune_job
    clear_keentune_env
    [ -n "$nr_open_default" ] && sysctl -w fs.nr_open=$nr_open_default
}
