# openssl
## Description

OpenSSL is an open-source toolkit that implements SSL (Secure Sockets Layer) and TLS (Transport Layer Security)
protocols. This test profile makes use of the built-in "openssl speed" benchmarking capabilities.

## Homepage
http://www.openssl.org/


## Category
performance

## Parameters

- nr_task:  线程数目
- type:     算法类型
- duration: 运行时长

## Results

```
rsa4096_sign: 403.4 sign/s
rsa4096_verify: 26654.7 verify/s
sha256_16bytes: 182738.61 kbyte/s
...

```

## Manual Run
