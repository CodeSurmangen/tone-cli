#!/bin/env python3

import sys
import os
import os.path
import json
import pandas as pd

result_data_path = str(sys.argv[1])
if result_data_path is None or result_data_path == "":
    print("please provide the result data path")
    sys.exit()

average_list = []
for current_dir, _, file_list in os.walk(result_data_path):
    for filename in file_list:
        filename_path = os.path.join(current_dir, filename)
        with open(filename_path) as f:
            for line in f.readlines():
                if line.strip() != "":
                    json_line = json.loads(line)
                    image = json_line["bench"]
                    pull_elapsed = float(json_line["pull_elapsed"])
                    average_list.append(
                        {
                            "image": image,
                            "pull": pull_elapsed,
                        }
                    )

average_df = pd.DataFrame(average_list)
for image, data in average_df.groupby("image"):
    mean_time =  data["pull"].mean()
    image = image.replace(":", "-")
    print("{}: {} seconds".format(image, mean_time))
