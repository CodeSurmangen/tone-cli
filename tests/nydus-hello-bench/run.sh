#!/bin/bash

source ${TONE_BM_RUN_DIR}/image-service/tests/bats/common_tests.sh

rust_toolchain=$(get_rust_toolcahin "${TONE_BM_RUN_DIR}/image-service")
compile_image="localhost/compile-image:${rust_toolchain}"
local_registry_name="myregistry"
local_registry_port="5000"


start_local_docker_registry()
{
    if docker ps | grep $local_registry_name | grep $local_registry_port; then
        echo "local registry container is running"
    else
        docker ps -a | grep -q $local_registry_name && docker rm -f $local_registry_name
        docker run -d --restart=always -p $local_registry_port:$local_registry_port -v /myregistry:/var/lib/registry --name $local_registry_name registry
        if [ $? -ne 0 ]; then
            echo "fail to start registry container"
            exit 1
        fi
    fi
}

build_docker_image()
{
    local dockerfile="/tmp/rust_golang_dockerfile"
    generate_rust_golang_dockerfile $dockerfile $rust_toolchain
    yum install -y docker
    docker build -f $dockerfile -t $compile_image .
    if [ $? -ne 0 ]; then
        echo "fail to build docker image"
        exit 1
    fi
}

compile_nydusd()
{
    docker run --rm -v ${TONE_BM_RUN_DIR}/image-service:/image-service $compile_image bash -c 'cd /image-service && make clean && make release'
    if [ -f "${TONE_BM_RUN_DIR}/image-service/target/release/nydusd" ] && [ -f "${TONE_BM_RUN_DIR}/image-service/target/release/nydus-image" ]; then
        /usr/bin/cp -f ${TONE_BM_RUN_DIR}/image-service/target/release/nydusd /usr/local/bin/
        /usr/bin/cp -f ${TONE_BM_RUN_DIR}/image-service/target/release/nydus-image /usr/local/bin/
    else
        echo "cannot find nydusd binary or nydus-image binary"
        exit 1
    fi
}

compile_nydus_snapshotter()
{
    rm -rf /var/lib/containerd/io.containerd.snapshotter.v1.nydus
    rm -rf /var/lib/nydus/cache
    docker run --rm -v ${TONE_BM_RUN_DIR}/nydus-snapshotter:/nydus-snapshotter $compile_image bash -c 'cd /nydus-snapshotter && make clean && make'
    if [ -f "${TONE_BM_RUN_DIR}/nydus-snapshotter/bin/containerd-nydus-grpc" ]; then
        /usr/bin/cp -f ${TONE_BM_RUN_DIR}/nydus-snapshotter/bin/containerd-nydus-grpc /usr/local/bin/
    else
        echo "cannot find containerd-nydus-grpc binary"
        exit 1
    fi
}

run()
{
    start_local_docker_registry
    build_docker_image
    compile_nydusd
    compile_nydus_snapshotter
    run_nydus_snapshotter ${TONE_BM_RUN_DIR}/nydus-snapshotter.log
    config_containerd_for_nydus

    export round_number=${round_number:-1}
    export image_list=${image_list:-""}
    cd ${TONE_BM_RUN_DIR}/hello-bench
    if [ -z "$image_list" ]; then
        ./run.sh -o push -p ./image_list.txt -t localhost:$local_registry_port
        ./run.sh -o convert -p ./image_list.txt -t localhost:$local_registry_port
        ./run.sh -o run -p ./image_list.txt -t localhost:$local_registry_port -r $round_number
    else
        ./run.sh -o push -i ${image_list} -t localhost:$local_registry_port
        ./run.sh -o convert -i ${image_list} -t localhost:$local_registry_port
        ./run.sh -o run -i ${image_list} -t localhost:$local_registry_port -r $round_number
    fi
}

teardown()
{
    systemctl stop nydus-snapshotter.service || true
    if ps -ef | grep containerd-nydus-grpc | grep -v grep; then
        ps -ef | grep containerd-nydus-grpc | grep -v grep | awk '{print $2}' | xargs kill -9
    fi
    if ps -ef | grep nydusd | grep fscache; then
        ps -ef | grep nydusd | grep fscache | awk '{print $2}' | xargs kill -9
    fi
    if mount | grep 'erofs on'; then
        mount | grep 'erofs on' | awk '{print $3}' | xargs umount
    fi
    /usr/bin/cp ${TONE_BM_RUN_DIR}/*.log $TONE_CURRENT_RESULT_DIR/ || true
    /usr/bin/cp -r ${TONE_BM_RUN_DIR}/hello-bench/data $TONE_CURRENT_RESULT_DIR/ || true
    /usr/bin/cp ${TONE_BM_RUN_DIR}/hello-bench/bench.json $TONE_CURRENT_RESULT_DIR/ || true
}

parse()
{
    $TONE_BM_SUITE_DIR/parse.py ${TONE_BM_RUN_DIR}/hello-bench/data
}
