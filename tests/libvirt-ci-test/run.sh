#!/bin/bash
# Avaliable environment:
BUILD_DIR=$TONE_BM_RUN_DIR/build

setup(){
    pip3 install docutils
}

run() {
    # check configure
    if ! meson setup $BUILD_DIR; then
        echo "====FAIL: libvirt-build"
        exit 1
    fi
    # check compile
    if ninja -C $BUILD_DIR; then
        echo "====PASS: libvirt-build"
    else
        echo "====FAIL: libvirt-build"
        exit 1
    fi
}

parse() {
    $TONE_BM_SUITE_DIR/parse.awk
}