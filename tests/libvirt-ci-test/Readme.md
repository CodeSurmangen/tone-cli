# libvirt-ci-test
## Description

This test suite is used for compiling, building tests and basic functional tests of the libvirt source code repository.

## Homepage
[https://gitee.com/anolis/libvirt](https://gitee.com/anolis/libvirt)

## Version

## Category

functional

## Parameters

## Results

```
libvirt-build: Pass
```

## Manual Run
```
step 1: tone run libvirt-ci-test

```

