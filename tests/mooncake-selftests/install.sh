#!/bin/bash
INSTALL_DEP=${INSTALL_DEP:-"0"}

build()
{
    GIT_SSH_COMMAND='ssh -i /root/.ssh/id_rsa'
    git clone ${CI_REPO_URL} $TONE_BM_BUILD_DIR/mooncake
    cd $TONE_BM_BUILD_DIR/mooncake
    git fetch origin ${CI_REPO_TARGET_BRANCH} :target_branch
    git checkout target_branch
    git reset --hard ${CI_REPO_TARGET_COMMIT}
    git fetch origin ${CI_REPO_SOURCE_BRANCH} :source_branch
    git checkout source_branch
    git reset --hard ${CI_REPO_SOURCE_COMMIT}
    git rebase target_branch
    if [[ $? != 0 ]]; then
        echo "rebase fail!"
        return -1
    fi
    if [[ "$INSTALL_DEP" == 1 ]]; then
        bash dependencies.sh
    fi
    mkdir build
    cd build
    cmake ..
    make -j
}
 
install()
{
    cp -a $TONE_BM_BUILD_DIR/* $TONE_BM_RUN_DIR
}