#!/bin/bash
 
setup()
{
    if ! which etcd | grep -q etcd; then
        apt install -y etcd
        systemctl restart networkd-dispatcher.service
        systemctl restart unattended-upgrades.service
    fi
}
 
run()
{
    export LD_LIBRARY_PATH=/usr/local/lib/
    [ -f $TONE_BM_RUN_DIR/results.txt ] && rm $TONE_BM_RUN_DIR/results.txt
    touch $TONE_BM_RUN_DIR/results.txt
    if [[ "$test" == "ci-test" ]]; then
        cd $TONE_BM_RUN_DIR/mooncake/.ci
        for case in $(ls | grep .sh); do
            chmod +x "$case"
            ./$case $TONE_BM_RUN_DIR/mooncake/build $TONE_BM_RUN_DIR
        done
    fi
}
 
 
parse()
{
    cat $TONE_BM_RUN_DIR/results.txt
}