#!/bin/bash
# Avaliable environment:
# PKG_CI_REPO_SOURCE_BRANCH=devel-8.2
# WEB_URL="https://mirrors.aliyun.com/tinycorelinux/14.x/x86_64/archive/14.0/TinyCorePure64-14.0.iso"
GIT_URL=${PKG_CI_REPO_SOURCE_URL:-"https://gitee.com/anolis/qemu-kvm.git"}
BRANCH=${PKG_CI_REPO_SOURCE_BRANCH:-"a8-virt-stream-an-source"}
DEP_PKG_LIST="git python38 ninja-build glib2-devel glibc-static flex bison pixman-devel expect nmap-ncat perl-CPAN libslirp-devel libslirp"

if [ "$PKG_CI_REPO_TARGET_BRANCH" == "devel-8.2" ]; then
    DEP_PKG_LIST=$(echo $DEP_PKG_LIST | sed 's/python38//g; s/libslirp-devel//g' | xargs)
fi

build()
{
    :
}

install()
{
    cp -Trf $TONE_BM_BUILD_DIR $TONE_BM_RUN_DIR
}
    
