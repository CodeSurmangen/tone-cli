# qemu-ci-test
## Description

This test suite is used for compiling and building tests, startup tests, and basic functional tests of the qemu-kvm source code repository.

## Homepage
[https://gitee.com/anolis/qemu-kvm/](https://gitee.com/anolis/qemu-kvm/)

## Version

## Category

functional

## Parameters
The following parameters support custom input:

- __IMAGE_URL__ : download address of qcow image
- __SSH_USER__ : default login username for qcow image
- __SSH_PASS__ : default login password for qcow image


## Results

```
qemu-kvm-build: Pass
qemu-kvm-version: Pass
qemu-kvm-unittests: Pass
qemu-kvm-boot: Pass
```

## Manual Run
```
step n: xxxx

```

