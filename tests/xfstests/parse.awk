#!/usr/bin/awk -f

BEGIN { FS="[/|.|\t| ]" }

/\[expunged\]|\[not run\]/ {
        printf("%s/%s: Skip\n",$1,$2)
        next
}

$NF ~ /^[0-9]+s/{
        printf("%s/%s: Pass\n",$1,$2)
        next
}

/^Failures:/ {
        split($0,fails," ")
        for(i=2;i<=length(fails);i++){
            printf("%s: Failed\n",fails[i])
        }
        next
}
