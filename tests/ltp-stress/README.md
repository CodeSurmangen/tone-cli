# ltp-stress

## Description

ltp stress is a system test suite developed by the Linux Test Project. It develops a test portfolio based on the utilization statistics of system resources to provide sufficient pressure for the system. Judge the stability and reliability of the system through pressure test.

## Homepage
[https://gitee.com/anolis/ltp.git](https://gitee.com/anolis/ltp.git)

Mirrored from upstream LTP project:
[https://github.com/linux-test-project/ltp](https://github.com/linux-test-project/ltp)

## Category
functional

## Manual Run
```
Cpu压力：50%的cpu
Memory压力：50%的memory，nr_cpu/4个进程并发消耗内存
Io压力：1个进程重复写1GB大小的文件

nr_cpu=$(nproc)
mem_kb=$(grep ^MemTotal /proc/meminfo | awk '{print $2}')
./runltp \
 -c $((nr_cpu / 2)) \
 -m $((nr_cpu / 4)),1,$(((mem_kb / 2) / (nr_cpu / 4) * 1024)) \
 -D 1,1,0,1 \
 -B ${LTP_DEV_FS:-ext4} \
 -R -p -q \
 -N \
 -t 168h \
 -d ${LTP_TMPDIR:-/tmp/ltp_tmpdir}
```
