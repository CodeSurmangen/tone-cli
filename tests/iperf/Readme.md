# iperf
## Description
	iperf is a tool for active measurements of the maximum achievable bandwidth on IP networks. It supports tuning of various parameters  related to timing, protocols, and buffers. For each test it reports the bandwidth, loss, and other parameters.

## Homepage
	https://iperf.fr/

## Version
	3.1.4

## Parameters
	- runtime 
	- protocol
	- cpu_affinity

## Category
	performation

## Results
	udp.bps:1048552.92551
	tcp.receiver.bps: 16706720000.0 , tcp.sender.bps: 16710030000.0

## Manual Run

