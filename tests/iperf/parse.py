#!/bin/env python
import json 
import os

iperfProtocol = os.environ.get("protocol")
resultDir = os.environ.get("TONE_CURRENT_RESULT_DIR")
resultFilePath = os.path.join(resultDir, "iperf.json")
with open(resultFilePath,'r') as f:
    results = json.load(f)

targetJson=results.get("end").get("streams")[0]
if 'udp' == iperfProtocol:
    result=targetJson.get(iperfProtocol).get("bits_per_second")
    print("{}.bps: {}".format(iperfProtocol,result))
else:
    senderResult=targetJson.get("sender").get("bits_per_second")
    receiverResult=targetJson.get("receiver").get("bits_per_second")
    print("{}.sender.bps: {}".format(iperfProtocol,senderResult))
    print("{}.receiver.bps: {}".format(iperfProtocol,receiverResult))
