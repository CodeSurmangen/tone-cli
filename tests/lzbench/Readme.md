Description
lzbench is an in-memory benchmark of open-source LZ77/LZSS/LZMA compressors.

Homepage
https://github.com/inikep/lzbench.git

Version
1.8.1

Category
performance

Parameters
- Algo: 被指定的算法

#Results
```
lz4_1.9.2.decompress: 3426.42
lz4_1.9.2.compress: 579.91
lz4_1.9.2.ratio: 47.60

```

