DEP_PKG_LIST="patch make gcc rpcbind parted gcc-c++"
#GIT_URL="https://gitee.com/rtoax/lmbench.git"
LMBENCH_VER="lmbench-3.0-a9"
WEB_URL="https://ostester.oss-cn-hangzhou.aliyuncs.com/benchmarks/${LMBENCH_VER}.tar"

build() {
    cd ${LMBENCH_VER}
    rpcs=$(rpm -ql glibc-headers | grep 'rpc/rpc.h' | wc -l)
    if [[ $rpcs -lt 1 ]];then
        if [[ "ubuntu debian uos kylin" =~ $TONE_OS_DISTRO ]];then
            install_pkg libtirpc-dev
        else
            install_pkg libtirpc-devel
        fi
        patch -p1 <$TONE_BM_SUITE_DIR/rpc_patch/fix_rpc_error.patch
    fi
    patch -p1 < $TONE_BM_SUITE_DIR/0001-add-loongarch64-support-for-lmbench.patch

    if [[ $(arch)=="aarch64" ]];then
        sed -i 's/arm\*/aarch64\*/' scripts/gnu-os
    fi
    make
}

install() {
    cp -rf * $TONE_BM_RUN_DIR/
}
