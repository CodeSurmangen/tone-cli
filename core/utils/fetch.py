#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib2
from shutil import copyfileobj
from log import logger


def download(url, filename):
    try:
        src = urllib2.urlopen(url)
    except Exception as e:
        logger.error(e)
        return None

    try:
        with open(filename, "wb") as dest:
            copyfileobj(src, dest)
    except Exception as e:
        logger.error(e)
    finally:
        src.close()
