#!/usr/bin/env python
# -*- coding: utf-8 -*-

import psutil


def get_memory(is_str=True):
    """Get momory"""
    mem_total = psutil.virtual_memory().total
    if is_str:
        return "{}G".format(mem_total >> 30)
    else:
        return (mem_total, "")


def get_meminfo():
    """Get memory info from /proc/meminfo

    :return: dict of memory info
    """
    with open('/proc/meminfo') as memfile:
        mem_info = memfile.readlines()
    info = {}
    for line in mem_info:
        if not line.strip():
            continue
        (item, content) = line.strip().split(":")
        result = content.split()
        unit = ""
        if len(result) > 1:
            unit = result[1]
        info[item] = {"value": int(result[0]), "unit": unit}

    return info
